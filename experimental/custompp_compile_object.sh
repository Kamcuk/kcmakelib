#!/bin/bash
set -euo pipefail

: "${CUSTOMPP_DEBUG:=0}"

fatal() {
	echo "ERROR: ${BASH_SOURCE##*/}:${BASH_LINENO[0]}: $*" >&2
	exit 1
}
is_debug() {
	((CUSTOMPP_DEBUG))
}
debug() {
	if is_debug; then
		echo "$*" >&2
	fi
}
logoutrun() {
	debug "+ ${*:1:$#-1} > ${*: -1}"
	"${@:1:$#-1}" > "${@: -1}"
}
logrun() {
	debug "+ $*" >&2
	"$@"
}
debugfile() {
	if is_debug; then
		echo "===> ${1:-stdin} <==="
		cat "${1:--}"
		echo "==== EOF"
	fi
}

###############################################################################

# printf "|%s|\n" "$0" "$@" | paste -sd ' '

# Parse arguments.
while getopts i:d:s:m:D opt; do
	case "$opt" in
	i) c_compiler_id=$OPTARG; ;;
	s) sourcefile=$OPTARG; : "${g_deptarget:-$sourcefile}"; ;;
	d) g_depfile=$OPTARG; ;;
	t) g_deptarget=$OPTARG; ;;
	m) g_mode=$OPTARG; ;;
	D) CUSTOMPP_DEBUG=1; ;;
	?) fatal "Invalid argument: $OPTING"; ;;
	esac
done
shift "$((OPTIND - 1))"

# Check compiler - it most probably will not work with non-GNU compilers.
case "$c_compiler_id" in
GNU) ;;
*) fatal "Unsupported compiler type: $c_compiler_id"; ;;
esac

# If mode is not given from the command line, detect it from file extension.
if [[ -z "${g_mode:-}" ]]; then
	sourcefileext="${sourcefile##*.}"
	case "$sourcefileext" in
		m4*) g_mode=m4; ;;
		j2*) g_mode=jinja2; ;;
		*) fatal "Invalid mode in filename: $sourcefile - $sourcefileext"; ;;
	esac
fi

# Determine the output filename to be passed to the compiler.
outputfileext=$(
	tmp=${sourcefile##*.}
	case "${tmp}" in
	(??c|c) echo c; ;;
	(??c*|c*) echo cpp; ;;
	(*) fatal "Invalid outputfileext filename: $tmp"; ;;
	esac
)

# Remove source file from the compiler command.
if [[ "${*: -1}" != "$sourcefile" ]]; then
	fatal "Last argument to compiler is not source file"
fi
g_cmd=("${@:1:$#-1}")

# Extract include paths from compiler arguments. We are handling -I only.
g_includes=()
while (($#)); do
	case "$1" in
	-I) g_includes+=("$1" "$2"); shift; ;;
	-I*) g_includes+=("$1"); ;;
	esac
	shift
done

# Create a temporary file with proper extension in the same directory as original file.
g_tempfile=$(mktemp -p "$(dirname "$(readlink -f "$sourcefile")")" --suffix=".$outputfileext")
# The file will be removed when were done.
trap 'rm "$g_tempfile"' EXIT

###############################################################################

# The mode function substitutes all `#include "file.m4h"` or `#include <file.j2c>`
# with the preprocessed content of the file
# and also they write the included file to depfile to track dependencies.

g_regex='[ \t]*#[ \t]*include[ \t]*(["<])([^">]*)[">][ \t]*(//[ \t]*CUSTOMPP|/\*[ \t]*CUSTOMPP[ \t]*\*/)[ \t]*'

mode_m4() {
	# We use _M4_ prefix for our symbols.
	m4_script=$(cat <<'EOF'
m4_define(`_M4_c', `')m4_dnl
m4_dnl
m4_dnl A function to get dirname from path. Should work like dirname
m4_define(`_M4_dirname',
		`m4_ifelse(
			m4_regexp($1, `/'),
			-1,
			`.',
			m4_patsubst($1, `/[^/]*$', `'))')m4_dnl
m4_dnl Note - newlines below are literal
m4_dnl Match beginning of the line or a newline
m4_dnl followed by #include <stuff.m4[ch]>
m4_dnl followed by end of file or a newline.
m4_define(`_M4_parse_include', `m4_patsubst(
	m4_include($1),
	`\(^\|
\)@REGEX@\(
\|$\)', `\1`'_M4_c(
	Get the path to include depending if it is relative path or not.
)m4_define(`_M4_sourcefile', 
		`m4_ifelse(
			\2`'m4_regexp(\3, `^/'),
			`"-1',
			_M4_dirname('_M4_sourcefile`)/\3,
			\3)')_M4_c(
	Actually include the file.
)_M4_parse_include(_M4_sourcefile)_M4_c(
	Include matches newline
)')')m4_dnl
_M4_parse_include(_M4_sourcefile)m4_dnl
EOF
	)
	# Basic regex wants (a|b) to be \(a\|b\)
	g_regex=${g_regex//(/\\(}
	g_regex=${g_regex//)/\\)}
	g_regex=${g_regex//|/\\|}
	#
	m4_script=${m4_script//@REGEX@/$g_regex}
	if ((${g_m4_noprefix:-0})); then
		m4_script=${m4_script//m4_}
	fi
	#
	cmd=(m4 -di -P "${g_includes[@]}" -D _M4_sourcefile="$sourcefile")
	debug "+ ${cmd[*]} > $g_tempfile"
	if ! err=$( { "${cmd[@]}" >"$g_tempfile" <<<"$m4_script" ; } 2>&1 ) <<<"$m4_script"; then
		# Replace error message with stdin with something more pointing to a file.
		sed "/^m4debug: /d; s ^m4:stdin:[0-9]\+: $sourcefile:\ m4: " <<<"$err" >&2
		exit 1
	fi
	# Matches m4debug dependency messages.
	deprgx='^m4debug: input read from \(.\+\..\+\)$'
	sed "/$deprgx/!d;"' s//\1/; /stdin/d' <<<"$err" > "$g_depfile"
}

mode_jinja2() {
	python_script=$(cat <<'EOF'
import argparse
import sys

def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)

parser = argparse.ArgumentParser()
parser.add_argument('-I', '--includes', nargs='*', default=[])
parser.add_argument('-t', '--tempfile', type=argparse.FileType('w'))
parser.add_argument('-i', '--sourcefile', type=argparse.FileType('r'))
parser.add_argument('-d', '--depfile', type=argparse.FileType('w'))
args = parser.parse_args()
print(args)

import re
import jinja2
import os
from jinja2.exceptions import TemplateSyntaxError
from jinja2.ext import Extension
from jinja2.lexer import count_newlines
from jinja2.lexer import Token

regex = r'@REGEX@'
# eprint(regex)
regexobj = re.compile(regex)

class CPPInclude(Extension):
	def handle_regex_match(self, type, inc, file):
		# When type is "inc" and path is not absolute
		if inc[0] != "/" and type == "\"":
			# Handle relative include paths like a pro.
			ret = os.path.join(os.path.dirname(file), inc)
			# This is not perfect - "inc" includes are __only__ searched relative.
			# You must be explicit.
		else:
			ret = inc
		return "{% include '" + ret + "' %}"
	def preprocess(self, source, name, filename=None):
		if filename:
			args.depfile.write(filename + "\n")
		ret = regexobj.sub(
			lambda x: self.handle_regex_match(x.group(1), x.group(2), filename),
			source)
		return ret

reldir = os.path.dirname(args.sourcefile.name)
args.includes += [ reldir, "/" ]
loader = jinja2.FileSystemLoader(
	args.includes,
	followlinks=True,
)
env = jinja2.Environment(loader=loader)
env.add_extension(CPPInclude)
args.tempfile.write(
	env.get_template(args.sourcefile.name).render()
)
EOF
	)
	python_script=${python_script//@REGEX@/$g_regex}
	logrun python - "${g_includes[@]}" -d "$g_depfile" -t "$g_tempfile" -i "$sourcefile" <<<"$python_script"
}

###############################################################################

case "$g_mode" in
m4) mode_m4; ;;
jinja2) mode_jinja2; ;;
*) exit 1; ;;
esac

# Dependency file contains included files one per line.
readarray -t mydeps < "$g_depfile"
# Add sourcefile for sure to dependencies.
mydeps+=("$sourcefile")
if is_debug; then
	# Add yourself to recompile when this script changes.
	mydeps+=("$(readlink -f "$0")")
fi

if is_debug; then
	declare -p mydeps
fi
debugfile "$g_tempfile"

# Run the real compiler with the temporary preprocessed file.
logrun "${g_cmd[@]}" "$g_tempfile"

# Remove tempfile from depfile.
ccdeps=$(sed "s \ $g_tempfile\   " "$g_depfile")
# Append to depfile additional files we included.
{
	# Do not print newline! - newline removed by $(...)
	printf "%s" "$ccdeps"
	for i in "${mydeps[@]}"; do
		printf ' \\'"\n"" %s" "$i"
	done
	echo
} > "$g_depfile"
debugfile "$g_depfile"

