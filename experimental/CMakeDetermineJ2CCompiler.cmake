include(CMakeDetermineCCompiler)
include(Compiler/J2C_lang)
set(CMAKE_${_CUSTOMPP_LANG}_COMPILER_NAMES "${_CUSTOMPP_lang}")
set(CMAKE_${_CUSTOMPP_LANG}_COMPILER "${CMAKE_CURRENT_LIST_DIR}/custompp_compile_object.sh")
set(CMAKE_${_CUSTOMPP_LANG}_COMPILER_ENV_VAR "${_CUSTOMPP_LANG}C")
configure_file(
  ${CMAKE_CURRENT_LIST_DIR}/CMake${_CUSTOMPP_LANG}Compiler.cmake.in
  ${CMAKE_PLATFORM_INFO_DIR}/CMake${_CUSTOMPP_LANG}Compiler.cmake
  @ONLY
)
include(Compiler/J2C_unlang)
