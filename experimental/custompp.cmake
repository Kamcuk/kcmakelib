
set(CUSTOMPP_SCRIPT ${CMAKE_CURRENT_LIST_DIR}/custompp_compile_object.sh)

# cmake-lint: disable=C0301,C0103
function(custompp_hook lang mode)
  set(langs C CPP)
  if(NOT lang IN_LIST langs)
    message(FATAL_ERROR "${lang} not in ${langs}")
  endif()
  set(modes m4 jinja2)
  if(NOT mode IN_LIST modes)
    message(FATAL_ERROR "${mode} not in ${modes}")
  endif()
  if(NOT PROJECT_NAME)
    message(FATAL_ERROR "Call custompp_hook _after_ project()")
  endif()

  find_program(bash bash)
  set(CMAKE_${lang}_COMPILE_OBJECT
    "${bash} ${CUSTOMPP_SCRIPT} -m ${mode} -i ${CMAKE_${lang}_COMPILER_ID} -d <DEP_FILE> -s <SOURCE> -- ${CMAKE_${lang}_COMPILE_OBJECT}"
    PARENT_SCOPE
  )
  # message(STATUS "CMAKE_${lang}_COMPILE_OBJECT=${CMAKE_${lang}_COMPILE_OBJECT}")
endfunction()

