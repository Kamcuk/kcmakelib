#!/bin/bash
case "${1:-}" in
shell) ;;
lint) section=cmake_lint; ;;
test|"") section=cmake_test; ;;
*)
	echo "Invalid mode." >&2
	echo "Usage: $0 [lint|test]" >&2
	exit 1
	;;
esac
set -x
docker run -ti --rm -v "$PWD:/work" -w /work alpine:edge sh -xc '
	apk add yq bash
	if [ -n $1 ]; then
		bash -xc "$(yq eval .$1.script .gitlab-ci.yml | cut -c3-)"
	fi
	bash
' _ "$section"

