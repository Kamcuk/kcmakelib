This is m4 library with common functions.

Some of them were copied from documentation, some were added.

The most important is, that this library uses `«` and `»` as opening and closing quotes. You have to call this before including the library:

```
m4_changequote(`«', `»')
```

They are very easy to type, on English and Polish keyboard settings on Linux - just press `Alt+(` and `Alt+)`.

