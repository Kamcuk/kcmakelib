
«
/**
 * Just ignore the arguments.
 */
#define m4_ignore(...)
»
m4_define(«m4_ignore», «»)  m4_dnl;

«
/**
 * Just ignore the arguments.
 * Used for commenting code.
 */
#define m4_ign(...)
»
m4_define(«m4_ign», «») m4_dnl;
