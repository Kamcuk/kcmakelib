m4_include(k.m4)
m4_divert(-1)

m4_define(«m4_do»,
	«m4_ifelse(
		«$#», «0», «»,
		«$#», «1», «m4_ifelse(«$1», «», «», $1)»,
		«$1$0(m4_shift($@))»)»)
m4_define(«m4_call», «$1(m4_reverse(m4_shift(m4_reverse(m4_shift($@)))))»)
m4_define(«m4_cat», «$1$0(m4_shift($@))»)

m4_define(«m4_assert_fail»,
	«m4_fatal(
		«Assertion at »m4___file__:m4___line__« failed: »m4_if(
		)«"$1" == "$2": "»$1«" != "»$2«": »m4_shift(m4_shift($@)))»)
m4_define(«m4_assert»,
	«m4_ifelse(
		m4_quote($1),
		m4_quote($2),
		«»,
		«m4_fatal($@)»)»)
m4_define(«m4_assert_not»,
	«m4_ifelse(
		m4_quote($1),
		m4_quote($2),
		«m4_fatal($@)»)»)
m4_define(«m4_assert_regex»,
	«m4_assert_not(
		«m4_regexp(«$1», «$2»)»,
		«-1»,
	)»)

m4_define(«m4_test»,
	«m4_ifdef(
		«m4_TEST»,
		«m4_assert($@)»)»)
m4_define(«m4_I», «m4_dnl«»»)
m4_define(«m4_S»,
«#line m4_eval($2 + m4___line__) "m4___file__"m4_ifelse(«$1», «», «», «$1
#line m4_eval($2 + m4___line__ + m4_count_lines(«$1»)) "m4___file__"
»)»)
m4_define(«m4_sdivert», «m4_divert(m4_ifelse($#, 0, 0, $@))m4_S(,1)m4_dnl»)
m4_define(«_m4_shquote», «'m4_patsubst(«$1», «'», «'\\''»)'»)
m4_define(«m4_shquote», «m4_applyforeachq(«_m4_shquote», «$@», « »)»)
m4_define_function(«m4_erun», «m4_esyscmd(m4_shquote($@))»)
m4_define_function(«m4_run», «m4_syscmd(m4_shquote($@))»)
m4_define_function(«m4_redir», «m4_syscmd(m4_shquote(m4_shift($@)) « >> » m4_shquote(«$1»))»)
m4_define_function(«m4_write», «m4_redir(«$1», «printf», «%s», «$2»)»)
m4_define_function(«m4_read», «m4_erun(«cat», «$1»)»)
m4_define(«m4_nl», «
»)
m4_define(«m4_regexquote», «m4_patsubst(«$@», «[]\/$*.^[]», «\&»)»)
m4_define(«m4_if», «m4_ifelse(«$@»)»)
m4_define_function(«m4_ifmath(expr, true, false)»,
	«m4_ifelse(m4_eval(«$1»), 0, «$3», «$2»)»)
m4_define_function(«m4_ifregex(str, rgx, true, false)»,
	«m4_ifelse(m4_regexp(«$1», «$2»), -1, «$4», «$3»)»)
m4_define_function(«m4_ifsubstr(str, needle, true, false)»,
	«m4_ifregex(«$1», m4_regexquote(«$2»), «$3», «$4»)»)
m4_define_function(«m4_max(numbers...)»,
	«m4_case(«$#»,
		«0», «»,
		«1», «$1»,
		«2», «m4_ifmath(«$1 > $2», «$1», «$2»)»,
		«$0($0($1, $2), m4_shift(m4_shift($@)))»)»)
m4_define(«m4_sdef», «m4_define(«$1», m4_S(«$2»))»)
m4_define_function(«m4_rstrip(str)», «m4_patsubst(«$1», «\s*$»)»)
m4_define_function(«m4_lstrip(str)», «m4_patsubst(«$1», «^\s*»)»)
m4_define_function(«m4_strip(str)», «m4_rstrip(m4_lstrip(«$1»))»)
m4_define(«m4_count», «m4_len(m4_patsubst(«$1», ««^$2»*»))»)
m4_define(«m4_count_lines», «m4_count(«$@», m4_nl)»)
m4_define(«m4_case»,
	«m4_ifelse(
		«$#», 0, «»,
		«$#», 1, «»,
		«$#», 2, «$2»,
		«$1», «$2», «$3»,
		«$0(«$1», m4_shift(m4_shift(m4_shift($@))))»)»)
m4_define(«m4_argn»,
	«m4_ifelse(
		«$1», 1, ««$2»»,
  		«m4_argn(m4_decr(«$1»), m4_shift(m4_shift($@)))»)»)
m4_define(«m4_casearg»,
	«m4_case(
		m4_shift($@),
		«m4_fatal(«$1: invalid number of arguments: $2»)»)»)
m4_define(«m4_split»,
	«m4_casearg(«$0», «$#»,
		«1», «$0(«$1», «\s»)»,
		«2», «m4_patsubst(
				m4_patsubst(
					«$1»,
					«[$2]*\([^$2]+\)[$2]*»,
					««««\1»»,»»),
				«,$»)»)»)
m4_define(«m4_join»,
	«m4_ifelse(
		«$#», «2», ««$2»»,
  		«ifelse(«$2», «», «», ««$2»_»)$0(«$1», m4_shift(m4_shift($@)))»)»)
m4_define(«_m4_join»,
	«m4_ifelse(«$#$2», «2», «»,
  		«m4_ifelse(«$2», «», «», ««$1$2»»)$0(«$1», m4_shift(m4_shift($@)))»)»)
# joinall(sep, args) - join each ARG, including empty ones,
# into a single string, with each element separated by SEP
m4_define(«m4_joinall», ««$2»_$0(«$1», m4_shift($@))»)
m4_define(«_m4_joinall»,
	«m4_ifelse(«$#», «2», «», ««$1$3»$0(«$1», m4_shift(m4_shift($@)))»)»)

m4_assert(«m4_max(1, 2, 3, 4)», 4)

m4_define(«m4_shsplit_sh»,
	«m4_patsubst(
		m4_esyscmd(«printf "%s" »m4_shquote($1)« | xargs printf "«««%s»»,»"»),
		«,$»)»)

m4_define_function(«m4_stridx(str, idx)», «m4_substr(«$1», «$2», 1)»)

m4_define_function(«m4_args_pop_back(cnt, args...)»,
	«m4_ifelse(
		«$1», 0, «m4_shift($@)»,
		«m4_reverse(
			m4_args_pop_front(
				$1,
				m4_reverse(
					m4_shift($@))))»)»)
m4_define_function(«m4_args_pop_front(cnt, args...)»,
	«m4_ifelse(
		«$1», 0, «$@»,
		«$1», 1, «m4_shift(m4_shift($@))»,
		«m4_args_pop_front(m4_eval($1 - 1), m4_shift(m4_shift($@)))»)»)
m4_define_function(«m4_args_len(args...)», «$#»)
m4_define_function(«m4_args_sub(start, len, args...)»,
	«m4_args_pop_back(
		m4_max(m4_eval($# - 2 - $1 - $2), 0),
		m4_args_pop_front($1, m4_shift(m4_shift($@))))»)


m4_define_function(«m4_array_new(name)», «m4_define(«$1», «»)»)
m4_define_function(«m4_array_add(name, elem)»,
	«m4_define(«$1»,
		m4_ifelse(
			m4_quote($1),
			«»,
			«m4_dquote(m4_dquote($2))»,
			«m4_dquote($1,m4_dquote($2))»))»)
m4_define_function(«_m4_array_len(args...)», «$#»)
m4_define_function(«m4_array_len(name)», «m4_args_len($1)»)
m4_define_function(«m4_array_idx(name, idx)», «m4_array_sub(«$1», «$2», 1)»)
m4_define_function(«m4_array_sub(name, start, len)»,
	«_m4_array_sub(«$2», «$3», $1)»)

m4_define(«m4_reverse»,
	«m4_ifelse(
		«$#», «0», «»,
		«$#», «1», ««$1»»,
        «m4_reverse(m4_shift($@)),«$1»»)»)


m4_define(«m4_args_first», «$1»)
m4_define_function(«_m4_func_push(args, ...)»,
	«m4_ifelse(
		m4_args_len($1), 0, m4_assert(«$#», «
		m4_pushdef(
		m4_args_first($1),
		$2)_m4_func(m4_popdef(
			m4_args_first$1)»
m4_define(«m4_func»,
	«m4_do(
		«m4_assert_regex(
			«$1»,
			«[A-Za-z0-9_]+\((\(\s*[a-z]*\s*,\)?\(\s*...\s*\)?)\)+»,
		)»,
		«m4_define(
			m4_patsubst(«$1», «(.*»), m4_dnl )
			«m4_patsubst(
				«$1», «[^(]*»)
		»,
	)»)


m4_sdivert(0)
