#include <stdio.h>
#include "inc/relinc2.h" // CUSTOMPP
static void relinc1() {
#if M4
	m4_define(`g_relinc1', `relinc1')m4_dnl
	printf("g_relinc1 ");
#elif JINJA2
	{% set g_relinc1 = "relinc1" -%}
	printf("{{ g_relinc1 }} ");
#endif
	relinc2();
}
