static void relinc2() {
#if M4
	m4_define(`g_relinc2', `relinc2')
	printf("g_relinc2 ");
#elif JINJA2
	{% set g_relinc2 = "relinc2" -%}
	printf("{{ g_relinc2 }} ");
#endif
}
