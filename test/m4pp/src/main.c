#include "inc/relinc1.h" // CUSTOMPP
#include <stdio.h>
#include <inc1.h> // CUSTOMPP
#include "inc/realrelinc1.h"
int main() {
	realrelinc1();
	inc1();
	relinc1();
#if M4
	m4_define(`g_text', `main')
	printf("g_text\n");
#elif JINJA2
	{% set g_text = "main" -%}
	printf("{{ g_text }}\n");
#endif
}

//// PASS_REGULAR_EXPRESSION realrelinc1 inc1 relinc1 relinc2 main

