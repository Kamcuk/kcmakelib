
static void inc1() {
#if M4
	m4_define(`g_inc1', `inc1')
	printf("g_inc1 ");
#elif JINJA2
	{% set g_inc1 = "inc1" -%}
	printf("{{ g_inc1 }} ");
#endif
}

