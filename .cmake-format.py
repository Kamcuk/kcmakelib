
with section("lint"):
    disabled_codes = ['C0111']
    local_var_pattern = '[a-z_][a-z0-9_]*'
    public_var_pattern = '[a-zA-Z_][a-zA-Z0-9_]*'
with section("format"):
    line_width = 120
with section("markup"):
    enable_markup = False
    first_comment_is_literal = True

